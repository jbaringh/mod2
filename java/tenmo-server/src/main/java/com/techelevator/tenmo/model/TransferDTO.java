package com.techelevator.tenmo.model;

import java.math.BigDecimal;

public class TransferDTO {

   int transferType;
   String toAccount;
   BigDecimal amount;

   public int getTransferType() {
      return transferType;
   }

   public void setTransferType(int transferType) {
      this.transferType = transferType;
   }

   public String getToAccount() {
      return toAccount;
   }

   public void setToAccount(String toAccount) {
      this.toAccount = toAccount;
   }

   public BigDecimal getAmount() {
      return amount;
   }

   public void setAmount(BigDecimal amount) {
      this.amount = amount;
   }
}