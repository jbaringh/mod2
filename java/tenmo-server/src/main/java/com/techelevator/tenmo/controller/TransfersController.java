package com.techelevator.tenmo.controller;

import com.techelevator.tenmo.dao.AccountDAO;
import com.techelevator.tenmo.dao.TransferDAO;
import com.techelevator.tenmo.execption.InsufficientFundsException;
import com.techelevator.tenmo.model.Transfer;
import com.techelevator.tenmo.model.TransferDTO;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.security.Principal;
import java.util.List;

@PreAuthorize("isAuthenticated()")
@RestController

public class TransfersController {

   private TransferDAO transferDAO;
   private AccountDAO accountDAO;

   public TransfersController(TransferDAO transferDAO) {
      this.transferDAO = transferDAO;
   }

   @RequestMapping(value = "/transfers", method = RequestMethod.GET)
   public List<Transfer> getTransfers(Principal principal)
   {
      return transferDAO.getTransfers(principal.getName());
   }

   @RequestMapping(value = "/transfers/{id}", method = RequestMethod.GET)
   public Transfer getTransfer(@PathVariable int id)
   {
      return transferDAO.getTransfer(id);
   }

   @RequestMapping(value = "/transfer", method = RequestMethod.PUT)
   public BigDecimal transfer(@Valid @RequestParam(required = true) String toUser, @RequestParam(required = true) double amount, Principal principal) throws InsufficientFundsException {
      transferDAO.transfer(toUser, principal.getName(), amount);
      return null;
   }

   @ResponseStatus(HttpStatus.CREATED)
   @RequestMapping(value = "/transferdto", method = RequestMethod.PUT)
   public BigDecimal transferdto(@Valid  @RequestBody TransferDTO transferDTO, Principal principal) throws InsufficientFundsException {
      transferDAO.request(transferDTO.getToAccount(), principal.getName(), transferDTO.getAmount().doubleValue());
      return null;
   }

   @RequestMapping(value = "/requests", method = RequestMethod.PUT)
   public void request(@Valid Principal principal, @RequestParam(required = true) String fromUser, @RequestParam(required = true) double amount) {
      transferDAO.request(fromUser, principal.getName(), amount);
   }

   @RequestMapping(value = "/requests", method = RequestMethod.GET)
   public List<Transfer> getRequests(Principal principal)
   {
      return transferDAO.getRequests(principal.getName());
   }


}
